# proformajs-demo

A demo for proformajs built with vue3 and vite.  

The skeleton was created by running ``npm init vue@latest`` (v3.2.47)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### For production preview
```
npm run preview
```

### For deployment via gitlab-ci
```
npm run build
```
Note that this wont work in your development environment - use ``npm run preview`` instead.
