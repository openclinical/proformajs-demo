import { createApp } from 'vue'
import App from './App.vue'

import "bootstrap/dist/css/bootstrap.min.css"

import ProformajsVue from '@openclinical/proformajs-vue3'

// change look and feel
import "../node_modules/bootswatch/dist/flatly/bootstrap.min.css";

createApp(App).use(ProformajsVue).mount('#app')
